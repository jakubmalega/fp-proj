import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productsListResponseSubj = new BehaviorSubject<HttpResponse<Product[]>>(null);

  productList$ = this.productsListResponseSubj.pipe(
    map(resp => resp ? resp.body : null)
    );

  constructor(private http: HttpClient) { }

  public fetchProducts(): void {
    this.http.get<Product[]>(environment.serviceUrl + 'products' , {
      observe: 'response'
    }).subscribe(products => {
      this.productsListResponseSubj.next(products);
    });
  }

  public getProductById(id: string): Observable<Product> {
    return this.http.get<Product>(environment.serviceUrl + 'products/' + id);
  }
}
