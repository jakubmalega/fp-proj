import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Contact } from 'src/app/models/Contact';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  sendMessage(contact: Contact): Observable<Contact>
  {
    return this.http.post<Contact>(environment.serviceUrl + 'contact', contact);
  }
}
