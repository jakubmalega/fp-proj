import { Component, OnInit, } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';

import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'fp-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactSent = false;

  errorMessages = new Map([
    ['required', 'This field is required'],
    ['email', 'Please enter valid email address'],
    ['maxlength', 'Message cannot exceed 200 characters']
  ]);

  contactForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    message: new FormControl('', [Validators.maxLength(200)])
  });

  constructor(private bookService: ContactService, private router: Router) {
   }

  ngOnInit(): void {
  }

  submitForm(): void{
    this.contactForm.markAllAsTouched();
    if (this.contactForm.valid) {
      this.bookService.sendMessage(this.contactForm.getRawValue())
      .subscribe(() => this.contactSent = true);
    }
  }
}
