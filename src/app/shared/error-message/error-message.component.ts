import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'fp-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnChanges {

  @Input() error: ValidationErrors = null;
  @Input() messageMap: Map<string, string> = null;

  messages: string[] = [];

  show = false;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (typeof changes.error.currentValue !== 'undefined' && changes.error.currentValue !== null)
    {
      const array = Object.getOwnPropertyNames(changes.error.currentValue);

      this.messages = [];

      array.forEach(element => {
        if (this.messageMap.get(element) !== undefined)
        {
          this.messages.push(this.messageMap.get(element));
        }

      });

      this.show = true;
    }
    else
    {
      this.show = false;
    }
  }
}
