export interface Contact {
  url: string;
  message: string;
}
