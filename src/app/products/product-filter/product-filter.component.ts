import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fp-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit {

  @Output() save = new EventEmitter();

  searchText = '';

  constructor() { }

  ngOnInit(): void {
  }

}
