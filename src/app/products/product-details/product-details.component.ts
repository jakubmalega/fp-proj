import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/core/services/product.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/Product';

@Component({
  selector: 'fp-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  id: string;
  product$: Observable<Product>;

  constructor(private route: ActivatedRoute, private productService: ProductService) {
    this.id = route.snapshot.params.id;
    this.product$ = productService.getProductById(this.id);

   }

  ngOnInit(): void {
  }

}
