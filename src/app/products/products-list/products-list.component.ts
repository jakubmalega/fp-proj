import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/core/services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'fp-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  products$: Observable<Product[]>;

  constructor(private productService: ProductService) {
    this.products$ = productService.productList$;
    productService.fetchProducts();
   }

  ngOnInit(): void {
  }

}
